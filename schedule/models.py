from django.db import models

# Create your models here.
class Schedule(models.Model):
    schedule_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    lecturer = models.CharField(max_length=50)
    credits = models.IntegerField()
    description = models.TextField()
    period = models.CharField(max_length=50)
    classroom = models.CharField(max_length=50)

class Task(models.Model):
    task_id = models.AutoField(primary_key=True)
    name = name = models.CharField(max_length=50)
    due_date = models.DateTimeField()
    subject = models.ForeignKey(Schedule, on_delete=models.CASCADE)