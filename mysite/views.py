from django.shortcuts import render, redirect
from django.utils import timezone
from datetime import timedelta 

# Create your views here.
def index(request):
    return render(request, 'index.html')

def contact(request):
    return render(request, 'contact.html')

def profile_summary(request):
    return render(request, 'miniProfile.html')

def time(request, idx):
    context = {}

    # Get current time
    time = timezone.now() + timedelta(hours = int(idx))

    context['time'] = time
    return render(request, 'time.html', context)

def time_root(request):
    return redirect('time/0') #FORCE REDIRECT TO TIME/0 FOR BLANK URL PARAMS