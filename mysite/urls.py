from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('contact', views.contact, name='contact'),
    path('summary', views.profile_summary, name='summary'),
    path('time', views.time_root, name='time_root'),
    path('time/<str:idx>', views.time, name='time'),
]
