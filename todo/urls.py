from django.urls import path
from . import views

urlpatterns = [
    path('todo', views.todo, name='todo_root'),
    path('todo/delete/<int:todo_id>', views.delete, name='delete'),
]
