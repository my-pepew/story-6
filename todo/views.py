from django.shortcuts import render, redirect
from .models import find_all_todos, Todo, Event, remove_attendant

# Create your views here.
def todo(request):
    if request.method == 'POST':
        event_name = request.POST.get('event_name')
        if event_name is not None:
            event_obj = Event(event=event_name)
            event_obj.save()
            todo = Todo(event=event_obj)
            todo.save()
            return redirect('todo_root')
        attendee = request.POST.get('name')
        event = request.POST.get('event')
        event_obj = Event.objects.get(pk=event)
        obj = Todo(event=event_obj, attendee=attendee)
        obj.save()
        return redirect('todo_root')

    ctx = {}
    ctx['todos'] = find_all_todos()
    
    return render(request, 'todo.html', ctx)

def delete(request, todo_id:int):
    this_todo = Todo.objects.get(pk=todo_id)
    remove_attendant(this_todo)
    return redirect(request.META.get('HTTP_REFERER', '/'))