from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import todo
from .models import Event, Todo, remove_attendant
# Create your tests here.
class Story6Test(TestCase):
    def test_url(self):
        res = Client().get('/todo')
        self.assertEqual(res.status_code, 200)
        
    def test_resolving_root_function(self):
        found = resolve('/todo')
        self.assertEqual(found.func, todo)

    # test models
    def test_adding_event(self):
        event = Event(event='Makan-makan lur')
        event.save()
        self.assertEqual(Event.objects.all().count(), 1)
    
    def test_adding_todo(self):
        event = Event(event='Makan-makan lur')
        event.save()
        todo = Todo(event=event, attendee='saya saja')
        todo.save()
        self.assertEqual(Todo.objects.all().count(), 1)

    def test_view_making_event(self):
        res = Client().post('/todo', {'event_name':'Apa Aja'})
        self.assertRedirects(res, '/todo')

    def test_rendering_page(self):
        res = Client().get('/todo')
        self.assertTemplateUsed(res, 'todo.html')

    def test_deleting_attendant(self):
        event = Event(event='Makan-makan lur')
        event.save()
        todo = Todo(event=event, attendee='saya saja')
        todo.save()
        remove_attendant(todo)
        self.assertEqual(Todo.objects.all().count(), 0)

    def test_delete_endpoint(self):
        event = Event(event='Makan-makan lur')
        event.save()
        todo = Todo(event=event, attendee='saya saja')
        todo.save()
        res = Client().get('/todo/delete/1')
        self.assertEqual(res.status_code, 302)