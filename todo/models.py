from django.db import models

# Create your models here.
class Event(models.Model):
    event = models.CharField(max_length=50)

class Todo(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    attendee = models.CharField(max_length=50, null=True)

def find_all_todos():
    return Todo.objects.all()

def remove_attendant(req: Todo):
    req.delete()