run:
	python3 manage.py runserver

migrate:
	python3 manage.py makemigrations
	python3 manage.py migrate

test:
	coverage run manage.py test
	coverage report -m


